from django.db import models

# Create your models here.


class UserInfo(models.Model):
    # django 读到，即在数据库中生成表
    # id bigint auto_increment primary key 自动生成
    name = models.CharField(max_length=32)
    pwd  = models.CharField(max_length=32)
    age  = models.IntegerField()

class WinInfo(models.Model):
    canteen= models.CharField(max_length=32)
    window = models.CharField(max_length=32)


class Taste(models.Model):
    name = models.CharField(max_length=32)


class DishInfo(models.Model):
    name    = models.CharField(max_length=32)                                  # 菜名
    img     = models.ImageField(upload_to='dishPhotos', blank=True, null=True) # 图片
    intro   = models.CharField(max_length=2048)                               # 介绍
    type_CHOICES = ('1', 'breakfast'), ('2', 'dinner'), ('3', 'drink')
    dinner  = models.CharField(max_length=1, choices=type_CHOICES)             # 早，正，饮品
    avail_wins = models.ManyToManyField(WinInfo)                           # 食堂 + 窗口
    price   = models.DecimalField(max_digits=3, decimal_places=1)             # 价格
    # 偏辣，偏咸，清淡，偏甜，面食，炒菜，自选，素菜，荤菜
    staple  = models.CharField(max_length=1)        # '0' or '1' or '2' 面食，默认，炒菜
    self_service    = models.CharField(max_length=1)  # '0' or '1' 自选 非自选
    vegetarian      = models.CharField(max_length=1)    # '0' or '1' or '2' 荤，无所谓，素
    taste           = models.ManyToManyField(Taste)        # 偏辣，偏咸，清淡，偏甜


class CommentInfo(models.Model):
    user_id = models.IntegerField()
    dish_id = models.IntegerField()
    text    = models.CharField(max_length=2048)
    parent  = models.IntegerField(null=True)