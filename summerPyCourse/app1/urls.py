# books/urls.py
from django.urls import path

from app1 import views


urlpatterns = [
    path('test/', views.test),
    path('askUserInfo/', views.userinfo),
    path('singleDish/', views.subDishInfo),
    path('getdish/', views.getdish),
]
