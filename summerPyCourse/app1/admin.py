from django.contrib import admin

# Register your models here.
from .models import UserInfo, WinInfo, Taste, DishInfo

admin.site.register(UserInfo)
admin.site.register(WinInfo)
admin.site.register(Taste)
admin.site.register(DishInfo)
