from django.shortcuts import render, HttpResponse

# Create your views here.


def login(request):
    name = "data name"
    return render(request, "login.html", {"n1": name})


from app1.models import UserInfo, DishInfo, WinInfo, Taste, CommentInfo
import json


def test(request):
    if request.method == 'GET':
        return HttpResponse("hello GET")
    # return HttpResponse(request)
    data = json.loads(request.body)
    bookName = data['name']
    pwd      = data['author']
    # bookName = request.POST.get("name")
    # pwd      = request.POST.get("author")
    UserInfo.objects.create(name=bookName, pwd=pwd, age=10)
    return HttpResponse("hello")


def userinfo(request):
    users = UserInfo.objects.all()
    json_data = []
    for user in users:
        json_data.append({
            "name":user.name,
            "pwd":user.pwd,
            "age":user.age
        })
    return HttpResponse(json_data)


def subDishInfo(request):
    # image = request.FILES.get('dishPic')
    basicInfo = json.loads(request.POST.get('basicInfor'))
    tag       = json.loads(request.POST.get('tag'))

    name   = basicInfo['name']
    intro  = basicInfo['intro']
    dinner = basicInfo.dinner
    canteen= basicInfo.canteen
    window = basicInfo.window
    price  = basicInfo.price

    staple = tag.staple
    selfService = tag.selfService
    vegetarian   = tag.vegetarian
    taste        = tag.taste

    myType = '1'
    if type == '早餐':
        myType = '1'
    elif type == '正餐':
        myType = '2'
    else:
        myType = '3'

    canteen_list = ['新北食堂', '合一楼', '学二食堂', '美食苑']
    myCanteen = canteen_list[canteen]

    window_list = ['一号窗口','二号窗口','三号窗口','四号窗口','五号窗口','六号窗口',]
    myWin = window_list[window]

    myStaple = '0'
    if staple == '面食':
        myStaple = '0'
    elif staple == '其他':
        myStaple = '1'
    elif staple == '炒菜':
        myStaple = '2'

    if WinInfo.objects.filter(canteen=myCanteen, window=myWin).exist():
        win_info = WinInfo.objects.filter(canteen=myCanteen, window=myWin)[0]
    else:
        win_info = WinInfo(canteen=myCanteen, window=myWin)

    self_service = '0'
    if selfService == "非自选":
        self_service = '1'

    myVegetarian = '0'
    if vegetarian == '素菜':
        myVegetarian = '2'
    elif vegetarian == '其他':
        myVegetarian = '1'

    if Taste.objects.filter(name=taste).exist():
        taste_obj = Taste.objects.filter(name=taste)
    else:
        taste_obj = Taste(name=taste)

    dish = DishInfo(
        name = name,
        img  = image,
        intro= intro,
        type = myType,
        avail_wins = win_info,
        price = float(price),
        staple = myStaple,
        self_service = self_service,
        vegetarian = myVegetarian,
        taste = taste_obj,
    )
    dish.save()


def getComment(request):
    comment = 'a comment'
    user_id = 1
    dish_id = 1
    parent = -1
    comment = CommentInfo(
        comment = comment,
        user_id = user_id,
        dish_id = dish_id,
        parent  = parent,
    )
    comment.save()


def showComment(request):
    dish_id = 1
    comments_a = CommentInfo.objects.filter(dish_id=dish_id).all()
    comments_b = []
    for cmt_a in comments_a:
        cmt_a_id = cmt_a.id
        comments_b.append(CommentInfo.objects.filter(parent=cmt_a_id))
